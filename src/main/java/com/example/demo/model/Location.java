package com.example.demo.model;

public class Location {
    private double top;
    private double left;

    public Location(double top, double left) {
        this.top = top;
        this.left = left;
    }

    public double getTop() {
        return top;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public double getLeft() {
        return left;
    }

    public void setLeft(double left) {
        this.left = left;
    }
}
