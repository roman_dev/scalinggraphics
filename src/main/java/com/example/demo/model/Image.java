package com.example.demo.model;

public class Image {
    private String svgImage;
    private Location[] location;

    public Image(String svgImage, Location[] location) {
        this.svgImage = svgImage;
        this.location = location;
    }

    public String getSvgImage() {
        return svgImage;
    }

    public void setSvgImage(String svgImage) {
        this.svgImage = svgImage;
    }

    public Location[] getLocation() {
        return location;
    }

    public void setLocation(Location[] location) {
        this.location = location;
    }
}
