package com.example.demo.controllers;

import com.example.demo.model.Image;
import com.example.demo.model.Location;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Stream;


@Controller
public class ScalingController {

    private Image image;


    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        prepareModel();
        model.put("image", image);
        return "welcome";
    }

    private void prepareModel() {
        String svg = readFromFile("static/svg/heliocentric.svg");
        Location[] locations = new Location[]{
                new Location(20, 20),
                new Location(20, 60),
                new Location(60, 20),
                new Location(60, 60)};

        image = new Image(svg, locations);
    }

    private String readFromFile(String fileName) {
        try {
            Path path = Paths.get(getClass().getClassLoader()
                    .getResource(fileName).toURI());

            StringBuilder data = new StringBuilder();
            Stream<String> lines = Files.lines(path);
            lines.forEach(line -> data.append(line).append("\n"));
            lines.close();
            return data.toString();
        } catch (URISyntaxException | IOException ex) {
            System.out.print(ex.getCause().getMessage());
            return "";
        }
    }
}
